@extends('layout.master');
@section('judul')
    Halaman Pendaftaran
@endsection

@section('content')
        <h2>Buat Account Baru</h2>
        <h4>Sign Up Form</h4>
        <form action="/welcome" method="post">
            @csrf
            <label>First name :</label><br><br>
            <input type="text" name="firstName"><br><br>

            <label>Last name :</label><br><br>
            <input type="text" name="lastName"><br><br>

            <label>Gender</label><br><br>
            <input type="radio" name="gender" value="man">Man <br>
            <input type="radio" name="gender" value="woman">Woman <br>
            <input type="radio" name="gender" value="other">Other <br><br>

            <label>Nationality</label><br><br>
            <select name="kebangsaan">
            <option value="Indonesia">Indonesia</option>
            <option value="Amerika">Amerika</option>
            <option value="Inggris">Inggris</option>
            </select><br><br>

            <label>Language Spoken</label><br><br>
            <input type="checkbox" name="bahasa">Bahasa Indonesia <br>
            <input type="checkbox" name="bahasa">English <br>
            <input type="checkbox" name="bahasa">Arabic <br>
            <input type="checkbox" name="bahasa">Japanese <br><br>

            <label>Bio</label><br><br>
            <textarea name="bio" cols="30" rows="10"></textarea><br>

            <input type="submit" value="Sign Up">
        </form>
@endsection